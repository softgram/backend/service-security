package co.com.project.security.user.adapter.inbound.web.controller;

import co.com.project.common.adapter.inbound.web.dto.Response;
import co.com.project.security.user.adapter.inbound.mapper.MapStructDTOMapper;
import co.com.project.security.user.adapter.inbound.web.dto.UserDTO;
import co.com.project.security.user.application.port.UserUseCase;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

import static co.com.project.common.adapter.util.ResponseBuilder.success;

@RestController
@RequestMapping("/users")
@RequiredArgsConstructor
public class UserController {
    private final UserUseCase service;
    private final MapStructDTOMapper mapper;

    @GetMapping
    public Response<List<UserDTO>> findAll() {
        return success(service.findAll().stream().map(mapper::toDTO).toList());
    }

    @GetMapping("/{id}")
    public Response<UserDTO> findById(@PathVariable Long id) {
        return success(mapper.toDTO(service.findById(id)));
    }

    @PostMapping("/save")
    public Response<UserDTO> save(@Valid @RequestBody UserDTO user) {
        return success(mapper.toDTO(service.save(mapper.toDomain(user))));
    }

    @DeleteMapping("/{id}")
    public Response<Void> deleteById(@PathVariable Long id) {
        return success(() -> service.deleteById(id));
    }

    @GetMapping("/username/{username}")
    public Response<UserDTO> findByUsername(@PathVariable String username) {
        return success(mapper.toDTO(service.findByUsername(username)));
    }
}
