package co.com.project.security.user.adapter.outbound.persistance.entity;

import co.com.project.security.shared.adapter.outbound.persistance.entity.EntityBase;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Table;
import lombok.Getter;
import lombok.Setter;

@Entity
@Getter
@Setter
@Table(name = "USERS")
public class User extends EntityBase {
    @Column(name = "NAME")
    private String name;
    @Column(name = "LASTNAME")
    private String lastname;
    @Column(name = "USERNAME")
    private String username;
    @Column(name = "PASSWORD")
    private String password;
    @Column(name = "CELLPHONE")
    private String cellphone;
    @Column(name = "DIRECTION")
    private String direction;
}
