package co.com.project.security.user.adapter.outbound.mapper;

import co.com.project.security.user.domain.model.User;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface MapStructEntityMapper {
    User toDomain(co.com.project.security.user.adapter.outbound.persistance.entity.User entity);
    co.com.project.security.user.adapter.outbound.persistance.entity.User toEntity(User domain);
}
