package co.com.project.security.user.adapter.inbound.mapper;

import co.com.project.security.user.adapter.inbound.web.dto.UserDTO;
import co.com.project.security.user.domain.model.User;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface MapStructDTOMapper {
    User toDomain(UserDTO dto);
    UserDTO toDTO(User user);
}
