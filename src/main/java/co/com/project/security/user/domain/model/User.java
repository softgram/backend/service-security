package co.com.project.security.user.domain.model;

import co.com.project.security.shared.domain.model.Base;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
@SuperBuilder(builderMethodName = "init")
public class User extends Base {
    private String name;
    private String lastname;
    private String username;
    private String password;
    private String cellphone;
    private String direction;
}
