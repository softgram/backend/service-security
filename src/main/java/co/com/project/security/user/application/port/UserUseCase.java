package co.com.project.security.user.application.port;

import co.com.project.security.user.domain.model.User;

import java.util.List;

public interface UserUseCase {
    User save(User user);
    List<User> findAll();
    void deleteById(Long id);
    User findById(Long id);
    User findByUsername(String username);
}
