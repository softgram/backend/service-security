package co.com.project.security.user.domain.exception;

import co.com.project.common.domain.exception.NotFoundException;
import lombok.experimental.StandardException;

@StandardException
public class NotUserFoundException extends NotFoundException {
    public NotUserFoundException() {
        super("Not user found in server.");
    }
}
