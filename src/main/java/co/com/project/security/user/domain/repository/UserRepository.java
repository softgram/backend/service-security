package co.com.project.security.user.domain.repository;

import co.com.project.security.user.domain.model.User;

import java.util.List;

public interface UserRepository {
    User save(User user);
    List<User> findAll();
    void deleteById(Long id);
    User findById(Long id);
    User findByUsername(String username);
}
