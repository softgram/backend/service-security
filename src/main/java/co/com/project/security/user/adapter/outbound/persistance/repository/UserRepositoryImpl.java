package co.com.project.security.user.adapter.outbound.persistance.repository;

import co.com.project.security.user.adapter.outbound.mapper.MapStructEntityMapper;
import co.com.project.security.user.domain.exception.NotUserFoundException;
import co.com.project.security.user.domain.model.User;
import co.com.project.security.user.domain.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
@RequiredArgsConstructor
public class UserRepositoryImpl implements UserRepository {
    private final JPAUserRepository repository;
    private final MapStructEntityMapper mapper;

    @Override
    public User save(User user) {
        user.setPassword(new BCryptPasswordEncoder().encode(user.getPassword()));
        return mapper.toDomain(repository.save(mapper.toEntity(user)));
    }

    @Override
    public List<User> findAll() {
        return repository
                    .findAll()
                        .stream()
                            .map(mapper::toDomain).toList();
    }

    @Override
    public void deleteById(Long id) {
        repository.deleteById(id);
    }

    @Override
    public User findById(Long id) {
        return repository
                    .findById(id)
                        .map(mapper::toDomain)
                            .orElseThrow(NotUserFoundException::new);
    }

    @Override
    public User findByUsername(String username) {
        return repository
                    .findUserByUsername(username)
                        .map(mapper::toDomain)
                            .orElseThrow(NotUserFoundException::new);
    }
}
