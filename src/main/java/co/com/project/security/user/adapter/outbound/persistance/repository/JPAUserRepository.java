package co.com.project.security.user.adapter.outbound.persistance.repository;

import co.com.project.security.user.adapter.outbound.persistance.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface JPAUserRepository extends JpaRepository<User, Long> {
    Optional<User> findUserByUsername(String username);
}
