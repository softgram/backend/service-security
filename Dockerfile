FROM openjdk:20 as builder

WORKDIR /app

COPY ./build/libs/service-security-1.0-SNAPSHOT.jar .

ENTRYPOINT java -jar ./service-security-1.0-SNAPSHOT.jar